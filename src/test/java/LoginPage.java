import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.FindBy;

/**
 * Created by it-school on 09.10.2017.
 *
 * создаем в main
 */
public class LoginPage {
    private final Webriver driver;
    public LoginPage(WebDriver driver){
        this.driver = driver;}


    @findBy(id="email")
    WebElement emailInput;

    @findBy(id="Button")
    WebElement submitButton;

    @findBy(xpath="//*[@name='pass']")
    WebElement passInput;

    public void login(String email, String pass){
        emailInput.sendKeys(email);
        passInput.sendKeys(pass);
        submitButton.click();



}
